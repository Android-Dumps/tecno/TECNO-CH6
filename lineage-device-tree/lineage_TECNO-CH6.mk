#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from TECNO-CH6 device
$(call inherit-product, device/tecno/TECNO-CH6/device.mk)

PRODUCT_DEVICE := TECNO-CH6
PRODUCT_NAME := lineage_TECNO-CH6
PRODUCT_BRAND := TECNO
PRODUCT_MODEL := TECNO CH6
PRODUCT_MANUFACTURER := tecno

PRODUCT_GMS_CLIENTID_BASE := android-transsion

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sys_tssi_64_tecno-user 11 RP1A.200720.011 185991 release-keys"

BUILD_FINGERPRINT := TECNO/CH6-OP/TECNO-CH6:11/RP1A.200720.011/211106V269:user/release-keys
